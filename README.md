# Terminal-Tools

Terminal Experimentations.
These programs have been designed to work with i3. They work fine on any terminal with python, but work better in terminal windows that are not full screen.

## Tarot Terminal
![Tarot Terminal](img/FU6yRSrXEAEXmFV.mp4)

A small python program that draws a number of cards at random from the major arcana of the tarot. 
The names of the drawn cards can be displayed written normally, or in 1337 speak, corresponding to the positive and negative versions of each card.

To launch a draw :
```
python3 tarot.py [number of cards to draw]
```
Example:
```
python3 tarot.py 3
```

## Crystal Ball Terminal
![Crystal Ball Terminal](img/FU6x-0FXEAYGDrm.mp4)

A small python program that displays a crystal ball on the screen, with pseudo-random perturbations in the ascii grid that composes it. The program is not made to work in full screen, even if the code adapts the ball to the size of the terminal. In big size, there may be some unpleasant blinks. The program works better when the terminal is reduced to a small window, 1/4 of the screen for example.

To launch  :
```
python3 crystal.py
```
