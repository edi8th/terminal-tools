import random, sys, time, os

# get the number of character fiting in the terminal width
try:
    col = os.get_terminal_size(0)[0]
except OSError:
    col = os.get_terminal_size(1)[0]

# declaration of some variables. hcol defines the height of the crystal square.
wael = []
mael = ""
above = ""
below = above
hcol=int(col/2.5)

#clearing terminal
os.system('clear')

#Filling the crystal with emptyness
for i in range(col):
    for j in range(hcol):
        wael.append(0)

#Repeated a lot
leaw=len(wael)

#We gonna make a lot of use of randomness. Plus, it alows you to modify it easily
def r(br,sx):
    aaa = random.randint(br,sx)
    return aaa

#Infinite loop of the cosmos
while above==below and below==above :
    for oo in range(1,100):
        if r(0,70)==0:
            wael[r(0,leaw-1)]=1

    for w in range(0, leaw-1):
        if wael[w] == 1:
            if r(0,r(1,5))==0:
                if w+1<=leaw:
                    wael[w+1]=1
            if r(0,r(1,5))==0:
                if w-1>0:
                    wael[w-1]=1
            if r(0,r(1,5))==0:
                if w+col<leaw:
                    wael[w+col]=1
            if r(0,r(1,5))==0:
                if w+col>0:
                    wael[w-col]=1
            if r(0,r(1,5))==0:
                wael[w]=0
        if wael[w] == 0:
            if r(0,4)==0:
                if w+1<=leaw:
                    wael[w+1]=0
            if r(0,4)==0:
                if w-1>0:
                    wael[w-1]=0
            if r(0,4)==0:
                if w+col<leaw:
                    wael[w+col]=0
            if r(0,4)==0:
                if w+col>0:
                    wael[w-col]=0
    for a in wael:
        if a==1:
            rr = r(0,2)
            if rr==0:
                mael+="\u001b[38;5;158m▓"
            elif rr==1:
                mael+="\u001b[38;5;231m▓"
            else:
                mael+="\u001b[38;5;159m▒"
            # if r(0,1)==0:
            #     mael+="\033[1;37;48m."
            # else:
            #     mael+="\033[1;35;48m."
        else:
            mael+="\u001b[38;5;225m░"
    print(mael)
    sc1, sc2=int(col/1.5), int(col/2)+1
    bsc1, bsc2= int((col-sc1)/2), int((col-sc2)/2)
    print("\u001b[38;5;175m "*bsc1+"─"*sc1)
    print(" "*bsc2+"─"*sc2)
    if col<20:
        print(" "*bsc2+"/"+" "*int(sc2-2)+"\\")
    else:
        print(" "*bsc2+"////"+" "*int(sc2-8)+"\\\\\\\\")
    mael=""
    # import sys
    # for i in range(0, 16):
    #     for j in range(0, 16):
    #         code = str(i * 16 + j)
    #         sys.stdout.write(u"\u001b[38;5;" + code + "m " + code.ljust(4))
    #     print (u"\u001b[0m")
    time.sleep(.06)
    os.system('clear')
